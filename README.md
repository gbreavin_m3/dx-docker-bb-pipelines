# dx-docker-bb-pipelines
 
Dockerfile to create basic image for use with SalesforceDX on CircleCI.

Lightweight Docker image using node alpine.

Includes:
- jq for shell JSON parsing
- bash (set as default as shell)
- grunt
- gettext for text file processing
- SalesforceDX CLI from NPM
- typescript