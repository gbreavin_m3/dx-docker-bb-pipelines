# use small node image
FROM node:alpine

# install jq for JSON parsing
RUN apk add --update --no-cache git bash jq gettext xmlstarlet

# switch to a bash terminal
RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

# install latest sfdx from npm
RUN npm install sfdx-cli --global
RUN npm install typescript --global
RUN npm install grunt --global
RUN sfdx --version
RUN sfdx plugins --core

# revert to low privilege user
USER node